package week4.Day1;

import java.io.IOException;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class HtmlReports {
	@Test
	public void reports() throws IOException
	{
	//creates an empty html file
	ExtentHtmlReporter html = new ExtentHtmlReporter("./Reports/extentReport.html");
	html.setAppendExisting(true);
	//creates an empty report
	ExtentReports extent = new ExtentReports();
	//attach reports to file
	extent.attachReporter(html);
	//creates a test
	ExtentTest test = extent.createTest("Login", "Login to Jquery");
	//assign author
	test.assignAuthor("Stephy");
	//assign category
	test.assignCategory("Sanity");
	//log the steps of test case -> Build comment below is to concatenate the string and media inside pass comment
	test.pass("Pass: User name entered", MediaEntityBuilder.createScreenCaptureFromPath(".././Snaps/img.png").build());
	test.fail("Fail: User name not entered", MediaEntityBuilder.createScreenCaptureFromPath(".././Snaps/img.png").build());
	//save file
	extent.flush();
	}
}
