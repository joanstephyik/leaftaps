package week2.Day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;


public class ProjectMethod extends SeMethod{
	@Parameters({"url","username","password"})
	@BeforeMethod
	public void loginMethod(String url, String username,String pword) {
		startApp("chrome", url);
		WebElement eleuserName = locateElement("id", "username");
		type(eleuserName, username);
		WebElement elepassword = locateElement("id", "password");
		type(elepassword, pword);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement eleCrmLink = locateElement("linktext", "CRM/SFA");
		click(eleCrmLink);
	}
	
	@AfterMethod
	public void logoutMethod()
	{
		closeBrowser();
	}

}








