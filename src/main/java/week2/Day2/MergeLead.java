package week2.Day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import testcases.ProjectMethod;



public class MergeLead extends ProjectMethod{
	String fromLeadId = "11310";
	String toLeadId = "11313";
	
	@Test
	public void mergeLeadMethod() throws InterruptedException
	{
		WebElement eleLeads = locateElement("xpath", "//a[@href='/crmsfa/control/leadsMain']");
		click(eleLeads);
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		WebElement eleMergeLead = locateElement("linktext", "Merge Leads");
		click(eleMergeLead);
		//entering first lead id 
		WebElement fromImg = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[1]");
		click(fromImg);
		switchToWindow(1);
		WebElement leadIDInWindow = locateElement("xpath", "//label[text()='Lead ID:']//following::input[1]");
		type(leadIDInWindow,fromLeadId);
		WebElement findLeadButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadButton);
		Thread.sleep(5000);
		//implicit wait didnt work here
		WebElement table = locateElement("xpath","(//table[@class='x-grid3-row-table'])");
		List<WebElement> rowlist = table.findElements(By.tagName("tr"));
		List<WebElement> collist = rowlist.get(0).findElements(By.tagName("td"));
		System.out.println(collist.get(0).getText());
		if (collist.get(0).getText().equals(fromLeadId)) 
		{
			collist.get(0).findElement(By.tagName("a")).click();
		}
		else
		{
			System.out.println("Lead id is not found");
		}

		switchToWindow(0);
		//entering second lead id 
		WebElement toImg = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[2]");
		click(toImg);
		switchToWindow(1);
		leadIDInWindow = locateElement("xpath", "//label[text()='Lead ID:']//following::input[1]");
		type(leadIDInWindow,toLeadId);
		findLeadButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadButton);
		Thread.sleep(5000);
		//implicit wait didnt work here
		table = locateElement("xpath","(//table[@class='x-grid3-row-table'])");
		rowlist = table.findElements(By.tagName("tr"));
		collist = rowlist.get(0).findElements(By.tagName("td"));
		System.out.println(collist.get(0).getText());
		if (collist.get(0).getText().equals(toLeadId)) 
		{
			collist.get(0).findElement(By.tagName("a")).click();
		}
		else
		{
			System.out.println("Lead id is not found");
		}
		switchToWindow(0);
		//click on merge button
		WebElement mergeButton = locateElement("class", "buttonDangerous");
		click(mergeButton);
		Thread.sleep(2000);
		acceptAlert();
		
		//xPath by text method for find leads link
		WebElement findLeadsLink = locateElement("xpath", "//a[text()='Find Leads']");
		click(findLeadsLink);
		Thread.sleep(1000);
		//Enter lead ID
		WebElement leadID = locateElement("xpath", "(//div[@class='x-tab-panel-bwrap']//input)[1]");
		type(leadID,fromLeadId);
		//Click on Find Leads button
		WebElement findLeadsButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadsButton);
		Thread.sleep(7000);
		WebElement noRec = locateElement("xpath", "//div[@class='x-paging-info']");
		String noRecMsg = getText(noRec);
		String expMsg = "No records to display";
		if (noRecMsg.equals(expMsg))
		{
			System.out.println("Passed No records to display is shown");
		}
		else
		{
			System.out.println("Failed: Some records are displayed");
		}

	}
	
}
