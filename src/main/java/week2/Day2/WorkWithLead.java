package week2.Day2;


//import java.util.ArrayList;
import java.util.List;
//import java.util.Set;
//import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class WorkWithLead extends SeMethod{
	//Create Lead Input
	String testCaseName = "duplicatelead";
	String strFirstName = "Stephen";
	String strLastName = "Edward";
	String strCompany = "TVS";
	String strUpdateCompany = "Wipro";
	String dateOfCurrentMonth = "20";
	//MErge lead input
	String fromLeadId = "10585";
	String toLeadId = "10592";

	@Test
	public void allTestCases() throws InterruptedException {
		//All test cases require login
		startApp("chrome", "http://leafTaps.com/opentaps");
		WebElement eleuserName = locateElement("id", "username");
		type(eleuserName, "DemoSalesManager");
		WebElement elepassword = locateElement("id", "password");
		type(elepassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);

		//According to test case run the corresponding method

		if (testCaseName.equals("createlead"))
		{
			createLead();
		}
		else if (testCaseName.equals("mergelead"))
		{
			mergeLead();
		}
		else if (testCaseName.equals("editlead"))
		{
			editLead();
		}
		else if (testCaseName.equals("duplicatelead"))
		{
			duplicateLead();
		}


	}
	public void createLead()
	{
		WebElement eleCrmLink = locateElement("linktext", "CRM/SFA");
		click(eleCrmLink);
		WebElement eleCreateLead = locateElement("linktext", "Create Lead");
		click(eleCreateLead);
		WebElement firstName = locateElement("id", "createLeadForm_firstName");
		type(firstName, strFirstName);
		WebElement lastName = locateElement("id", "createLeadForm_lastName");
		type(lastName, strLastName);
		WebElement companyName = locateElement("id", "createLeadForm_companyName");
		type(companyName, strCompany);

		WebElement sourceDrop = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingIndex(sourceDrop,3);
		WebElement market = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(market,"Automobile");
		WebElement dateImg = locateElement("id", "createLeadForm_birthDate-button");
		//Date Selection
		click(dateImg);
		WebElement calTable = locateElement("xpath","//div[@class='calendar']/table");
		List<WebElement> calRows = calTable.findElements(By.tagName("tr"));
		for (int j = 0; j < calRows.size(); j++) {
			List<WebElement> calCols = calRows.get(j).findElements(By.tagName("td"));
			for (int j2 = 0; j2 < calCols.size(); j2++) {
				String colVal = calCols.get(j2).getText();
				if (colVal.equals(dateOfCurrentMonth))
				{
					calCols.get(j2).click();
				}
			}
		}
		WebElement submit = locateElement("class", "smallSubmit");
		click(submit);

		//closeBrowser();
	}

	public void mergeLead() throws InterruptedException
	{
		WebElement eleCrmLink = locateElement("linktext", "CRM/SFA");
		click(eleCrmLink);
		WebElement eleLeads = locateElement("xpath", "//a[@href='/crmsfa/control/leadsMain']");
		click(eleLeads);
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		WebElement eleMergeLead = locateElement("linktext", "Merge Leads");
		click(eleMergeLead);
		//entering first lead id 
		WebElement fromImg = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[1]");
		click(fromImg);
		switchToWindow(1);
		WebElement leadIDInWindow = locateElement("xpath", "//label[text()='Lead ID:']//following::input[1]");
		type(leadIDInWindow,fromLeadId);
		WebElement findLeadButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadButton);
		Thread.sleep(5000);
		//implicit wait didnt work here
		WebElement table = locateElement("xpath","(//table[@class='x-grid3-row-table'])");
		List<WebElement> rowlist = table.findElements(By.tagName("tr"));
		List<WebElement> collist = rowlist.get(0).findElements(By.tagName("td"));
		System.out.println(collist.get(0).getText());
		if (collist.get(0).getText().equals(fromLeadId)) 
		{
			collist.get(0).findElement(By.tagName("a")).click();
		}
		else
		{
			System.out.println("Lead id is not found");
		}

		switchToWindow(0);
		//entering second lead id 
		WebElement toImg = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[2]");
		click(toImg);
		switchToWindow(1);
		leadIDInWindow = locateElement("xpath", "//label[text()='Lead ID:']//following::input[1]");
		type(leadIDInWindow,toLeadId);
		findLeadButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadButton);
		Thread.sleep(5000);
		//implicit wait didnt work here
		table = locateElement("xpath","(//table[@class='x-grid3-row-table'])");
		rowlist = table.findElements(By.tagName("tr"));
		collist = rowlist.get(0).findElements(By.tagName("td"));
		System.out.println(collist.get(0).getText());
		if (collist.get(0).getText().equals(toLeadId)) 
		{
			collist.get(0).findElement(By.tagName("a")).click();
		}
		else
		{
			System.out.println("Lead id is not found");
		}
		switchToWindow(0);
		//click on merge button
		WebElement mergeButton = locateElement("class", "buttonDangerous");
		click(mergeButton);
		Thread.sleep(2000);
		acceptAlert();
		
		//xPath by text method for find leads link
		WebElement findLeadsLink = locateElement("xpath", "//a[text()='Find Leads']");
		click(findLeadsLink);
		Thread.sleep(1000);
		//Enter lead ID
		WebElement leadID = locateElement("xpath", "(//div[@class='x-tab-panel-bwrap']//input)[1]");
		type(leadID,fromLeadId);
		//Click on Find Leads button
		WebElement findLeadsButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadsButton);
		Thread.sleep(7000);
		WebElement noRec = locateElement("xpath", "//div[@class='x-paging-info']");
		String noRecMsg = getText(noRec);
		String expMsg = "No records to display";
		if (noRecMsg.equals(expMsg))
		{
			System.out.println("Passed No records to display is shown");
		}
		else
		{
			System.out.println("Failed: Some records are displayed");
		}

	}

	public void editLead() throws InterruptedException
	{
		WebElement eleCrmLink = locateElement("linktext", "CRM/SFA");
		click(eleCrmLink);
		WebElement eleLeads = locateElement("xpath", "//a[@href='/crmsfa/control/leadsMain']");
		click(eleLeads);
		WebElement eleFindLeads = locateElement("xpath", "//a[text()='Find Leads']");
		click(eleFindLeads);
		WebElement eleFirstName = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(eleFirstName, strFirstName);
		WebElement eleButtonFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleButtonFindLeads);		
		//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		Thread.sleep(4000);
		//Identify the table and click on the first result
		WebElement table = locateElement("xpath","//table[@class='x-grid3-row-table']");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		//When used td.click, the link was not clicked. Only when traveled down to link it got clicked
		WebElement eleFirstResult = rows.get(0).findElements(By.tagName("td")).get(0).findElements(By.tagName("a")).get(0);
		click(eleFirstResult);
		WebElement title = locateElement("xpath", "//div[@class='frameSectionExtra']/preceding-sibling::div");
		String strTitle = getText(title);		
		String pageTitle = "View Lead";
		if (pageTitle.equals(strTitle))
		{
			System.out.println("Page title appeared is: "+strTitle+" as expected: "+pageTitle);
		}
		else
		{
			System.out.println("Page title is not correct "+strTitle);
		}	
		WebElement eleEditButton = locateElement("linktext", "Edit");
		click(eleEditButton);
		WebElement companyName = locateElement("xpath", "//span[@class='requiredField']/following::input[1]");
		companyName.clear();
		type(companyName, strUpdateCompany);
		WebElement updateButton = locateElement("xpath","//input[@value='Update']");
		click(updateButton);
		WebElement updatedComp = locateElement("id", "viewLead_companyName_sp");
		String companyActual = getText(updatedComp);
		if (companyActual.contains(strUpdateCompany))
		{
			System.out.println("Company name is changed as: "+companyActual);
		}
		else
		{
			System.out.println("Company name is not correct");
		}

	}

	public void duplicateLead() throws InterruptedException
	{
		WebElement eleCrmLink = locateElement("linktext", "CRM/SFA");
		click(eleCrmLink);
		WebElement eleLeads = locateElement("xpath", "//a[@href='/crmsfa/control/leadsMain']");
		click(eleLeads);
		WebElement eleFindLeads = locateElement("xpath", "//a[text()='Find Leads']");
		click(eleFindLeads);
		WebElement eleFirstName = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(eleFirstName, strFirstName);
		WebElement eleButtonFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleButtonFindLeads);		
		//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		Thread.sleep(4000);
		//Identify the table and click on the first result
		WebElement table = locateElement("xpath","//table[@class='x-grid3-row-table']");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		//When used td.click, the link was not clicked. Only when traveled down to link it got clicked
		WebElement eleFirstResult = rows.get(0).findElements(By.tagName("td")).get(0).findElements(By.tagName("a")).get(0);
		click(eleFirstResult);
		
		
		WebElement oldLeadIdEle = locateElement("id","viewLead_companyName_sp");
		String oldLeadId = getText(oldLeadIdEle);
		String[] split1 = oldLeadId.split("\\(");
		oldLeadId = split1[1].substring(0, 5);
		System.out.println("old lead "+oldLeadId);
		
		Thread.sleep(2000);
		WebElement eleDupButton = locateElement("linktext", "Duplicate Lead");
		click(eleDupButton);
		Thread.sleep(2000);
		WebElement title = locateElement("xpath", "//div[@class='frameSectionExtra']/preceding-sibling::div");
		String strTitle = getText(title);		
		String pageTitle = "Duplicate Lead";
		if (pageTitle.equals(strTitle))
		{
			System.out.println("Page title appeared is: "+strTitle+" as expected: "+pageTitle);
		}
		else
		{
			System.out.println("Page title is not correct "+strTitle);
		}	
		WebElement createLead = locateElement("xpath", "//span[contains(text(),'Postal Code')]/following::input[@value='Create Lead']");
		click(createLead);
		Thread.sleep(2000);
		WebElement newLeadIdEle = locateElement("id","viewLead_companyName_sp");
		String newLeadId = getText(newLeadIdEle);
		String[] split2 = newLeadId.split("\\(");
		newLeadId = split2[1].substring(0, 5);
		System.out.println("new lead "+newLeadId);
		
		if (!oldLeadId.equals(newLeadId)) 
		{
			System.out.println("Lead Ids are different");
		}
		else
		{
			System.out.println("Lead Ids are same");
		}
	}
}
