package week2.Day2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
//import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

public class SeMethod implements WdMethods{
	public static void name() {
		
	}
	public RemoteWebDriver driver ;
	public void startApp(String browser, String url) {
		if (browser.equals("chrome")) {			
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			ChromeOptions chrome = new ChromeOptions();
			chrome.addArguments("--disable-notifications");
			driver = new ChromeDriver(chrome);
		} else if (browser.equals("firefox")) {			
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
			driver = new FirefoxDriver();
		}		
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("The Browser "+browser+" launched Successfully");
	}

	
	public WebElement locateElement(String locator, String locValue) {
		
		switch (locator) {
		case "id": return driver.findElementById(locValue);
		case "class": return driver.findElementByClassName(locValue);
		case "linktext": return driver.findElementByLinkText(locValue);
		case "xpath": return driver.findElementByXPath(locValue);
		}		
		return null;
	}
	

	@Override
	public WebElement locateElement(String locValue) {
		// TODO Auto-generated method stub
		try {
			return driver.findElementById(locValue);
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			System.err.println("Element not found");
		}
		return null;
	}
	
	public List<WebElement> locateElements(String locator, String locValue) {
		
		switch (locator) {
		case "id": return driver.findElementsById(locValue);
		case "class": return driver.findElementsByClassName(locValue);
		case "linktext": return driver.findElementsByLinkText(locValue);
		case "xpath": return driver.findElementsByXPath(locValue);
		}		
		return null;
	}

	
	public void type(WebElement ele, String data) {
		ele.sendKeys(data);
		System.out.println("the Data "+data+" Endered Successfully");
	}

	@Override
	public void click(WebElement ele) {
	ele.click();
	System.out.println("the Element "+ele+" is clicked Successfully");
	}

	@Override
	public String getText(WebElement ele) {
		// TODO Auto-generated method stub
		String element = ele.getText();
		return element;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		// TODO Auto-generated method stub
		Select sel = new Select(ele);
		sel.selectByVisibleText(value);
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub
		Select sel = new Select(ele);
		sel.selectByIndex(index);
	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		// TODO Auto-generated method stub
		String actTitle = driver.getTitle();
		if (actTitle.equals(expectedTitle)) 
		{
			System.out.println("Actual title is "+actTitle+" is as expected"+expectedTitle);
			return true;
		}
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		try {
			if (expectedText.equals(ele.getText()))
			{
				System.out.println("Webelement text is as expected "+ele.getText());
			}
			else
			{
				System.out.println("Expected text" +expectedText+ "didnt match with the actual text" +ele.getText());
			}
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			System.err.println("Element not found");
		}
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		try {
			if (ele.getText().contains(expectedText))
			{
				System.out.println("Webelement contains the text "+expectedText+" expected "+ele.getText());
			}
			else
			{
				System.out.println("Actual text" +ele.getText()+ "didnt contain the actual text" +expectedText);
			}
		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			System.err.println("Element not found");
		}
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		if (ele.getAttribute(attribute).equals(value))
		{
			System.out.println("Attribute "+attribute+" matched with the value "+value);
		}
		else
		{
			System.out.println("Attribute "+attribute+" not matched with the value "+value);
		}
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		if (ele.getAttribute(attribute).contains(value))
		{
			System.out.println("Attribute "+attribute+" has the value "+value);
		}
		else
		{
			System.out.println("Attribute "+attribute+" does not the value "+value);
		}
	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		if (ele.isSelected())
		{
			System.out.println(ele.getText()+" selected");
		}
		else
		{
			System.out.println(ele.getText()+" Not selected");
		}
		
	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		if (ele.isDisplayed())
		{
			System.out.println("ELement displayed");
		}
		else
		{
			System.out.println("ELement Not displayed");
		}
	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> windlist = new ArrayList<>();
		windlist.addAll(windowHandles);
		driver.switchTo().window(windlist.get(index));
	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub
		driver.switchTo().frame(ele);
	}

	@Override
	public void acceptAlert() {
		// TODO Auto-generated method stub
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		Alert alert = driver.switchTo().alert();
		alert.dismiss();
	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		Alert alert = driver.switchTo().alert();
		String alertText = alert.getText();
		return alertText;
	}
	int i =1;
	@Override
	public void takeSnap() {
		// TODO Auto-generated method stub
		File src = driver.getScreenshotAs(OutputType.FILE);
		File fs = new File("./Snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, fs);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		// TODO Auto-generated method stub
		driver.close();
	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub
		driver.quit();
	}

}
