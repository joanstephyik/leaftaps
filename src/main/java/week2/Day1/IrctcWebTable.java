package week2.Day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class IrctcWebTable {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		//instantiate chrome object
		ChromeDriver driver = new ChromeDriver();
		//Launch URL
		driver.get("https://erail.in");
		driver.manage().window().maximize();
		//Enter from station and go to next field
		WebElement fromStation = driver.findElementById("txtStationFrom");
		fromStation.clear();
		fromStation.sendKeys("MAS",Keys.TAB);
		WebElement toStation = driver.findElementById("txtStationTo");
		toStation.clear();
		toStation.sendKeys("SBC",Keys.TAB);
		driver.findElementById("chkSelectDateOnly").click();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		//working with the webtable
		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		
		
		//For each row add the value in the second column to a list
		List<String> myList = new ArrayList<>();
		for (int j = 0; j < rows.size(); j++) 
		{
			List<WebElement> cols = rows.get(j).findElements(By.tagName("td"));
			String trainName = cols.get(1).getText();
			myList.add(trainName);
			
		}
		
		//click on Train name header to sort it
		driver.findElementByLinkText("Train Name").click();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		//After Sorting, a new table element has to be identified, otherwise stale element exception will be thrown
		WebElement table1 = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> rows1 = table1.findElements(By.tagName("tr"));
		Thread.sleep(3000);	
		//Take a new list from the UI
		List<String> myList1 = new ArrayList<>();
		for (int k = 0; k < rows1.size(); k++) 
		{
			List<WebElement> cols1 = rows1.get(k).findElements(By.tagName("td"));
			String trainName1 = cols1.get(1).getText();
			myList1.add(trainName1);
		}
		Collections.sort(myList);
		System.out.println("Sorted by Code\tSorted in UI");
		int flag = 0;
		//compare two lists
		for (int m = 0; m < myList.size(); m++)
		{
			
			if (myList.get(m).equals(myList1.get(m)))
			{
				//everytime when the value matches, it increments
				flag++;
				System.out.println(myList.get(m)+"\t"+myList1.get(m));	
			}
			else
			{
				System.out.println(myList.get(m)+"\t"+myList1.get(m));
			}
			
		}
		if(flag==myList.size())
		{
			//if everytime it got incremented, all values should be matched
			System.out.println("All values are matched");
		}
		else
		{
			System.out.println("Some values are mismatched");
		}
				
		
	}

}
