package week2.Day1;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WorkingWithWebTables {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		//instantiate chrome object
		ChromeDriver driver = new ChromeDriver();
		//Launch URL
		driver.get("http://leafground.com/pages/table.html");
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		//get into the table
		WebElement table = driver.findElementByXPath("//table");
		//collects rows
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		
		for (int row = 0; row < rows.size(); row++) {
			//collects columns
			List<WebElement> columns = rows.get(row).findElements(By.tagName("td"));
			for (int col = 0; col < columns.size(); col++) {
				if (columns.get(col).getText().equals("80%")) 
				{
					columns.get(col+1).click();
					break;
				}
			} 
		}	
		
	}

}
