package week2.Day1;



import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class WorkingWithDrops {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		//instantiate chrome object
		ChromeDriver driver = new ChromeDriver();
		//Launch URL
		driver.get("http://leafground.com/pages/Dropdown.html");
		driver.manage().window().maximize();
		//Thread.sleep(3000);
		Select dd1 = new Select(driver.findElementById("dropdown1"));
		dd1.selectByIndex(20);
		Select dd2 = new Select(driver.findElementByXPath("//select[@name='dropdown2']"));
		dd2.selectByVisibleText("Selenium");
		Select dd3 = new Select(driver.findElementByXPath("//select[@name='dropdown3']"));
		dd3.selectByValue("1");
		WebElement dd4 = driver.findElementByXPath("//select[@class='dropdown']");
		Select sel = new Select(dd4);
		//to get the number options in the drop down
		List<WebElement> options = sel.getOptions();
		System.out.println(options.size());
		//to print each option
		for (WebElement ls : options) {
			System.out.println(ls.getText());
		}
		WebElement dd5 = driver.findElementByXPath("//div[@class='example'][5]/select");
		dd5.sendKeys("L");
		Select multidd = new Select(dd5);
		boolean multiple = multidd.isMultiple();
		System.out.println("Is multiple? "+ multiple);
		//Multi select with Select method
		Select dd6 = new Select(driver.findElementByXPath("//div[@class='example'][6]/select"));
		dd6.selectByIndex(1);
		dd6.selectByValue("3");
		//Multi Select with Send Keys didnt work
		/*WebElement dd6 = driver.findElementByXPath("//div[@class='example'][6]/select");
		dd6.sendKeys("S",Keys.CONTROL,"U");*/
	}

}
