package week2.Day1;

import java.util.List;
//import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFindElements {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		//instantiate chrome object
		ChromeDriver driver = new ChromeDriver();
		//Launch URL
		driver.get("https://www.google.co.in");
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.findElementById("lst-ib").sendKeys("Stephy Joan",Keys.ENTER);
		List<WebElement> list = driver.findElementsByPartialLinkText("Stephy Joan");
		System.out.println("Number of links with my name: "+list.size());
		System.out.println("Printing all lists");
		for (WebElement webElement : list) {
			System.out.println(webElement.getText());
		}
	}

}
