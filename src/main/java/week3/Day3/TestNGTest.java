package week3.Day3;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class TestNGTest extends withoutTest {
  @Test
  public void testMethod() {
	  System.out.println("I am the test method");
  }
  @BeforeMethod
  public void beforeMethod() {
	  System.out.println("I am the before method");
  }

  @AfterMethod
  public void afterMethod() {
	  System.out.println("I am the afterMethod");
  }

  @BeforeClass
  public void beforeClass() {
	  System.out.println("I am the beforeClass");
  }

  @AfterClass
  public void afterClass() {
	  System.out.println("I am the afterClass");
  }

  @BeforeTest
  public void beforeTest() {
	  System.out.println("I am the beforeTest");
  }

  @AfterTest
  public void afterTest() {
	  System.out.println("I am the afterTest");
  }

  @BeforeSuite
  public void beforeSuite() {
	  System.out.println("I am the beforeSuite");
  }

  @AfterSuite
  public void afterSuite() {
	  System.out.println("I am the afterSuite");
  }

}
