package week3.Day3;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;


public class withoutTest {
	 
	 @BeforeMethod
	  public void beforeMethodWOTest() {
		  System.out.println("I am the before method of Parent Class");
	  }

	  @AfterMethod
	  public void afterMethodWOTest() {
		  System.out.println("I am the afterMethod of Parent Class");
	  }

	  @BeforeClass
	  public void beforeClassWOTest() {
		  System.out.println("I am the beforeClass of Parent Class");
	  }

	  @AfterClass
	  public void afterClassWOTest() {
		  System.out.println("I am the afterClass of Parent Class");
	  }
	
	  @BeforeTest
	  public void beforeTestWOTest() {
		  System.out.println("I am the beforeTest of Parent Class");
	  }

	  @AfterTest
	  public void afterTestWOTest() {
		  System.out.println("I am the afterTest of Parent Class");
	  }

	  @BeforeSuite
	  public void beforeSuiteWOTest() {
		  System.out.println("I am the beforeSuite of Parent Class");
	  }

	  @AfterSuite
	  public void afterSuiteWOTest() {
		  System.out.println("I am the afterSuite of Parent Class");
	  }
}
