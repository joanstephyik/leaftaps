package week3.Day1;

import org.openqa.selenium.Keys;
//import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import week2.Day2.SeMethod;

	public class SelectElements extends SeMethod {
	    @Test
		public  void selectable() {
			// TODO Auto-generated method stub
			startApp("chrome", "https://jqueryui.com/selectable/");
			driver.switchTo().frame(0);
			WebElement select1 = locateElement("xpath","//li[text()='Item 1']");		
			Actions build = new Actions(driver);
			build.sendKeys(Keys.CONTROL);
			build.click(select1).perform();
			WebElement select3 = locateElement("xpath","//li[text()='Item 3']");
			build.click(select3).perform();
			WebElement select5 = locateElement("xpath","//li[text()='Item 5']");
			build.click(select5).perform();
			build.release().perform();
		}
	}


