package week3.Day1;

import java.util.List;

import org.openqa.selenium.Keys;
//import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import week2.Day2.SeMethod;

public class WorkingWithNewTabs extends SeMethod {
	@Test
	public  void openMultiTabs() {
		// TODO Auto-generated method stub
		startApp("chrome", "https://www.indeed.co.in/Fresher-jobs");
		List<WebElement> allLinks = driver.findElementsByXPath("//a[@data-tn-element='jobTitle']");
		Actions build = new Actions(driver);		
		for (int i = 0; i < allLinks.size(); i++) {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			build.sendKeys(allLinks.get(i),Keys.CONTROL).click(allLinks.get(i)).perform();
			switchToWindow(i+1);
			String title = driver.getTitle();
			System.out.println("Title of tag number "+i+" :"+title);
			switchToWindow(0);
			
		}
		closeAllBrowsers();
	}
}
