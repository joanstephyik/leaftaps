package week3.Day1;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import week2.Day2.SeMethod;

public class DragElements extends SeMethod {
    @Test
	public  void draggable() {
		// TODO Auto-generated method stub
		startApp("chrome", "https://jqueryui.com/draggable/");
		driver.switchTo().frame(0);
		WebElement draggable = locateElement("id","draggable");
		Actions build = new Actions(driver);
		Point location = draggable.getLocation();
		int xVal = location.getX();
		xVal = xVal+20;
		int yVal = location.getY();
		yVal = yVal+50;
		build.dragAndDropBy(draggable,xVal,yVal).perform(); 				
	}

}
	