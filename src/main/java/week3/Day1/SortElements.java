package week3.Day1;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
//import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import week2.Day2.SeMethod;

public class SortElements extends SeMethod {
	@Test
	public  void sortable() throws InterruptedException, IOException {
		// TODO Auto-generated method stub
		startApp("chrome", "https://jqueryui.com/sortable/");
		driver.switchTo().frame(0);
		WebElement select1 = locateElement("xpath","//li[text()='Item 1']");
		WebElement select3 = locateElement("xpath","//li[text()='Item 3']");
		WebElement select4 = locateElement("xpath","//li[text()='Item 4']");
		WebElement select6 = locateElement("xpath","//li[text()='Item 6']");
		Actions build = new Actions(driver);
		Point location1 = select1.getLocation();
		Point location3 = select3.getLocation();
		Point location4 = select4.getLocation();
		Point location6 = select6.getLocation();
		System.out.println("Before Drag and drop 1 "+location1.getX()+","+location1.getY());
		System.out.println("Before Drag and drop 3 "+location3.getX()+","+location3.getY());
		System.out.println("Before Drag and drop 4 "+location4.getX()+","+location4.getY());
		System.out.println("Before Drag and drop 6 "+location6.getX()+","+location6.getY());
		build.dragAndDropBy(select1,location6.getX(),location6.getY()).perform();
		Thread.sleep(3000);
		//location1 = select1.getLocation();
		location3 = select3.getLocation();
		location4 = select4.getLocation();
		location6 = select6.getLocation();
		build.dragAndDropBy(select1,location1.getX(),location1.getY()).perform();
		Thread.sleep(3000);
		location1 = select1.getLocation();
		location3 = select3.getLocation();
		location4 = select4.getLocation();
		location6 = select6.getLocation();
		build.dragAndDropBy(select3,location4.getX(),location4.getY()).perform();
		Thread.sleep(3000);
		location1 = select1.getLocation();
		location3 = select3.getLocation();
		location4 = select4.getLocation();
		location6 = select6.getLocation();
		build.dragAndDropBy(select4,location3.getX(),location3.getY()).perform();
		Point location11 = select1.getLocation();
		Point location61 = select6.getLocation();
		Point location41 = select4.getLocation();
		Point location31 = select3.getLocation();
		System.out.println("After Drag and drop 1 "+location11.getX()+","+location11.getY());
		System.out.println("After Drag and drop 3 "+location31.getX()+","+location31.getY());
		System.out.println("After Drag and drop 4 "+location41.getX()+","+location41.getY());
		System.out.println("After Drag and drop 6 "+location61.getX()+","+location61.getY());
		File src = driver.getScreenshotAs(OutputType.FILE);
		File fs = new File("./Snaps/img.png");
		FileUtils.copyFile(src, fs);
		
	}
}
