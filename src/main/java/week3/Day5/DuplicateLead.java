package week3.Day5;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testcases.ProjectMethod;

public class DuplicateLead extends ProjectMethod {
	String strFirstName = "Stephen";
	@Test
	public void duplicateLeadMethod() throws InterruptedException
	{
		WebElement eleLeads = locateElement("xpath", "//a[@href='/crmsfa/control/leadsMain']");
		click(eleLeads);
		WebElement eleFindLeads = locateElement("xpath", "//a[text()='Find Leads']");
		click(eleFindLeads);
		WebElement eleFirstName = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(eleFirstName, strFirstName);
		WebElement eleButtonFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleButtonFindLeads);		
		//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		Thread.sleep(4000);
		//Identify the table and click on the first result
		WebElement table = locateElement("xpath","//table[@class='x-grid3-row-table']");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		//When used td.click, the link was not clicked. Only when traveled down to link it got clicked
		WebElement eleFirstResult = rows.get(0).findElements(By.tagName("td")).get(0).findElements(By.tagName("a")).get(0);
		click(eleFirstResult);
		
		
		WebElement oldLeadIdEle = locateElement("id","viewLead_companyName_sp");
		String oldLeadId = getText(oldLeadIdEle);
		String[] split1 = oldLeadId.split("\\(");
		oldLeadId = split1[1].substring(0, 5);
		System.out.println("old lead "+oldLeadId);
		
		Thread.sleep(2000);
		WebElement eleDupButton = locateElement("linktext", "Duplicate Lead");
		click(eleDupButton);
		Thread.sleep(2000);
		WebElement title = locateElement("xpath", "//div[@class='frameSectionExtra']/preceding-sibling::div");
		String strTitle = getText(title);		
		String pageTitle = "Duplicate Lead";
		if (pageTitle.equals(strTitle))
		{
			System.out.println("Page title appeared is: "+strTitle+" as expected: "+pageTitle);
		}
		else
		{
			System.out.println("Page title is not correct "+strTitle);
		}	
		WebElement createLead = locateElement("xpath", "//span[contains(text(),'Postal Code')]/following::input[@value='Create Lead']");
		click(createLead);
		Thread.sleep(2000);
		WebElement newLeadIdEle = locateElement("id","viewLead_companyName_sp");
		String newLeadId = getText(newLeadIdEle);
		String[] split2 = newLeadId.split("\\(");
		newLeadId = split2[1].substring(0, 5);
		System.out.println("new lead "+newLeadId);
		
		if (!oldLeadId.equals(newLeadId)) 
		{
			System.out.println("Lead Ids are different");
		}
		else
		{
			System.out.println("Lead Ids are same");
		}
		
	}
	
	@DataProvider(name="duplicateDataProvider")
	public Object[][] provideDataDuplicate()
	{
		Object[][] inputDuplicate = new Object[2][1];
		
		inputDuplicate[0][0] = "Catherine";
		
		
		inputDuplicate[1][0] = "Stephen";
		
			
		
		return inputDuplicate;
	}
	
	
}
