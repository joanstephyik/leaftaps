package week3.Day5;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


import testcases.ProjectMethod;

public class CreateLead extends ProjectMethod{

		String dateOfCurrentMonth = "15";
		
	@Test(dataProvider="createDataProvider")
	public void createLeadMethod(String fname, String sname, String cname, int sourceId)
	{	
		WebElement eleCreateLead = locateElement("linktext", "Create Lead");
		click(eleCreateLead);
		WebElement firstName = locateElement("id", "createLeadForm_firstName");
		type(firstName, fname);
		WebElement lastName = locateElement("id", "createLeadForm_lastName");
		type(lastName, sname);
		WebElement companyName = locateElement("id", "createLeadForm_companyName");
		type(companyName, cname);

		WebElement sourceDrop = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingIndex(sourceDrop,sourceId);
		WebElement market = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(market,"Automobile");
		WebElement dateImg = locateElement("id", "createLeadForm_birthDate-button");
		//Date Selection
		click(dateImg);
		WebElement calTable = locateElement("xpath","//div[@class='calendar']/table");
		List<WebElement> calRows = calTable.findElements(By.tagName("tr"));
		for (int j = 0; j < calRows.size(); j++) {
			List<WebElement> calCols = calRows.get(j).findElements(By.tagName("td"));
			for (int j2 = 0; j2 < calCols.size(); j2++) {
				String colVal = calCols.get(j2).getText();
				if (colVal.equals(dateOfCurrentMonth))
				{
					calCols.get(j2).click();
				}
			}
		}
		WebElement submit = locateElement("class", "smallSubmit");
		click(submit);

		//closeBrowser();
	}
	
	@DataProvider(name="createDataProvider")
	public Object[][] provideDataCreate()
	{
		Object[][] inputCreate = new Object[3][4];
		
		inputCreate[0][0] = "Stephy";
		inputCreate[0][1] = "Joan";
		inputCreate[0][2] = "INFOSYS";
		inputCreate[0][3] = 3;
		
		inputCreate[1][0] = "Stephen";
		inputCreate[1][1] = "Edward";
		inputCreate[1][2] = "CTS";
		inputCreate[1][3] = 4;
		
		inputCreate[2][0] = "Venba";
		inputCreate[2][1] = "StehenJoan";
		inputCreate[2][2] = "Govt Of India";
		inputCreate[2][3] = 2;				
		
		return inputCreate;
	}
}
