package week3.Day5;

import java.io.IOException;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelLearn {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		XSSFWorkbook wbook = new XSSFWorkbook("./data/LeadData.xlsx");
		XSSFSheet sheet = wbook.getSheet("data");
		//getLastRowNum returns the number of rows excluding first row
		int rowCount = sheet.getLastRowNum();
		System.out.println("number of rows "+rowCount);
		int colCount = sheet.getRow(0).getLastCellNum();
		System.out.println("number of cols "+colCount);
		for (int i = 1; i <=rowCount; i++) {
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j <= colCount; j++) {
				XSSFCell cell = row.getCell(j);
					if(row.getCell(j)==null)
					{
						//row.getCell(j).setCellType(cell.CELL_TYPE_STRING);
						continue;
					}
					else
					{
						CellType cellTypeEnum;
						cellTypeEnum = cell.getCellTypeEnum();

						if (cellTypeEnum==CellType.STRING)
						{
							System.out.println(cell.getStringCellValue());
						}
						else if (cellTypeEnum==CellType.NUMERIC)
						{
							System.out.println(""+(long)cell.getNumericCellValue());
						}

					}
				/*} catch (NullPointerException e) {
					// TODO Auto-generated catch block

					//cell.setCellValue("");
					System.err.println("Cell Value is Null");
					//e.printStackTrace();					
				}*/

			}
		}
		wbook.close();
	}

}
