package week3.Day5;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testcases.ProjectMethod;



public class EditLead extends ProjectMethod{
	
	
	@Test(dataProvider="editDataProvider")
	public void editLeadMethod(String strFirstName,String strUpdateCompany) throws InterruptedException
	{
		WebElement eleLeads = locateElement("xpath", "//a[@href='/crmsfa/control/leadsMain']");
		click(eleLeads);
		WebElement eleFindLeads = locateElement("xpath", "//a[text()='Find Leads']");
		click(eleFindLeads);
		WebElement eleFirstName = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(eleFirstName, strFirstName);
		WebElement eleButtonFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleButtonFindLeads);		
		//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		Thread.sleep(4000);
		//Identify the table and click on the first result
		WebElement table = locateElement("xpath","//table[@class='x-grid3-row-table']");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		//When used td.click, the link was not clicked. Only when traveled down to link it got clicked
		WebElement eleFirstResult = rows.get(0).findElements(By.tagName("td")).get(0).findElements(By.tagName("a")).get(0);
		click(eleFirstResult);
		WebElement title = locateElement("xpath", "//div[@class='frameSectionExtra']/preceding-sibling::div");
		String strTitle = getText(title);		
		String pageTitle = "View Lead";
		if (pageTitle.equals(strTitle))
		{
			System.out.println("Page title appeared is: "+strTitle+" as expected: "+pageTitle);
		}
		else
		{
			System.out.println("Page title is not correct "+strTitle);
		}	
		WebElement eleEditButton = locateElement("linktext", "Edit");
		click(eleEditButton);
		WebElement companyName = locateElement("xpath", "//span[@class='requiredField']/following::input[1]");
		companyName.clear();
		type(companyName, strUpdateCompany);
		WebElement updateButton = locateElement("xpath","//input[@value='Update']");
		click(updateButton);
		WebElement updatedComp = locateElement("id", "viewLead_companyName_sp");
		String companyActual = getText(updatedComp);
		if (companyActual.contains(strUpdateCompany))
		{
			System.out.println("Company name is changed as: "+companyActual);
		}
		else
		{
			System.out.println("Company name is not correct");
		}

	}
	
	@DataProvider(name="EditDataProvider")
	public Object[][] provideDataEdit()
	{
		Object[][] inputEdit = new Object[3][2];
		
		inputEdit[0][0] = "Stephy";
		inputEdit[0][1] = "Wipro";
				
		inputEdit[1][0] = "Stephen";
		inputEdit[1][1] = "Govt Of India";
				
		inputEdit[2][0] = "Venba";
		inputEdit[2][1] = "Govt of TamilNadu";
						
		
		return inputEdit;
	}

}
