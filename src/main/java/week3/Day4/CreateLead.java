package week3.Day4;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;


import week2.Day2.ProjectMethod;

public class CreateLead extends ProjectMethod{
	
	String strFirstName = "Catherine";
	String strLastName = "Jessica";
	String strCompany = "SRM";
	String strUpdateCompany = "Medical";
	String dateOfCurrentMonth = "15";
	
@Test
public void createLeadMethod()
{	
	WebElement eleCreateLead = locateElement("linktext", "Create Lead");
	click(eleCreateLead);
	WebElement firstName = locateElement("id", "createLeadForm_firstName");
	type(firstName, strFirstName);
	WebElement lastName = locateElement("id", "createLeadForm_lastName");
	type(lastName, strLastName);
	WebElement companyName = locateElement("id", "createLeadForm_companyName");
	type(companyName, strCompany);

	WebElement sourceDrop = locateElement("id","createLeadForm_dataSourceId");
	selectDropDownUsingIndex(sourceDrop,3);
	WebElement market = locateElement("id","createLeadForm_marketingCampaignId");
	selectDropDownUsingText(market,"Automobile");
	WebElement dateImg = locateElement("id", "createLeadForm_birthDate-button");
	//Date Selection
	click(dateImg);
	WebElement calTable = locateElement("xpath","//div[@class='calendar']/table");
	List<WebElement> calRows = calTable.findElements(By.tagName("tr"));
	for (int j = 0; j < calRows.size(); j++) {
		List<WebElement> calCols = calRows.get(j).findElements(By.tagName("td"));
		for (int j2 = 0; j2 < calCols.size(); j2++) {
			String colVal = calCols.get(j2).getText();
			if (colVal.equals(dateOfCurrentMonth))
			{
				calCols.get(j2).click();
			}
		}
	}
	WebElement submit = locateElement("class", "smallSubmit");
	click(submit);

	//closeBrowser();
}

}
