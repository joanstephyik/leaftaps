package week3.Day2;

//import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
//import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
//import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import week2.Day2.SeMethod;

public class ZoomCar extends SeMethod {
	@Test
	public  void startJourney() {
		// TODO Auto-generated method stub
		String pickUpPoint ="Thuraipakkam";
		startApp("chrome", "https://www.zoomcar.com/chennai");
		WebElement start = locateElement("xpath","//a[@title='Start your wonderful journey']");
		click(start);
		List<WebElement> allPickupPoints = locateElements("xpath", "//div[@class='items']");
		for (int i = 0; i < allPickupPoints.size(); i++) {
			if (allPickupPoints.get(i).getText().equals(pickUpPoint))
			{
				click(allPickupPoints.get(i));
				break;
			}
			else
			{
				System.out.println("Please provide a valid pick up point");
			}
		}
		WebElement next = locateElement("xpath","//button[text()='Next']");
		click(next);
		WebElement nextDay = locateElement("xpath","//div[@class='days']//div[2]");
		String expectedDay = nextDay.getText().replaceAll("//D", "");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		click(nextDay);
		click(next);
		WebElement nextDayActual = locateElement("xpath","//div[@class='days']//div[@class='day picked ']");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String actualDay = nextDayActual.getText();
		if (actualDay.equals(expectedDay))
		{
			System.out.println("Start Day "+actualDay+" is as entered "+expectedDay);
		}
		else
		{
			System.out.println("Start Day "+actualDay+" is Not as entered "+expectedDay);
		}
		WebElement done = locateElement("xpath","//button[text()='Done']");
		click(done);
		List<WebElement> allCars = locateElements("class","car-item");
		System.out.println("Number of cars listsed "+allCars.size());
		List<WebElement> amountList = locateElements("class","price");
		List<String> intAmountList = new ArrayList<>();
		for (WebElement eachEle : amountList) {
			
			intAmountList.add(eachEle.getText().replaceAll("\\D", ""));
			
		}
		
		String maxAmount = Collections.max(intAmountList);
		String carName =null;
		System.out.println("Maximum amount in list "+maxAmount);
		//WebElement Brandname = driver.findElementByXPath("//div[contains(text(),'"+maxAmount+"')]/preceding::h3[1]");
		for (int i = 0; i < amountList.size(); i++) {
			System.out.println(amountList.get(i).getText().replaceAll("\\D", ""));
			if (amountList.get(i).getText().replaceAll("\\D", "").equals(maxAmount))
			{
				carName = getText(allCars.get(i).findElement(By.tagName("h3")));
				//allCars.get(i).findElement(By.className("details")).click();
				//click(allCars.get(i).findElement(By.className("button")));
			}
		}
		System.out.println("Car with Maximum value "+carName);
		
	}
}
