package week3.Day2;

/*import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;*/
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import week2.Day2.SeMethod;

public class fabLikeTestLeaf extends SeMethod {
	@Test
	public  void startJourney() {
		// TODO Auto-generated method stub
		String searchItem = "TestLeaf";
		startApp("chrome", "https://www.facebook.com/");
		WebElement emailId = locateElement("email");
		type(emailId, "facestephy@gmail.com");
		WebElement passWord = locateElement("pass");
		type(passWord, "Stephen@1love");
		WebElement loginButton = locateElement("xpath","//input[@value='Log In']");
		click(loginButton);
		WebElement searchBox = locateElement("xpath", "//input[@name='q']");
		type(searchBox, searchItem);
		WebElement searchButton = locateElement("xpath", "//form[@action='/search/web/direct_search.php']//i");	
		click(searchButton);
		//searches for the text we search for
		WebElement searchResult = locateElement("xpath", "//span[text()='Pages' or 'Places']/following::div[text()='"+searchItem+"']");
		verifyDisplayed(searchResult);
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//identifies the like button under the section Pages or Places
		WebElement likeButton = locateElement("xpath", "//span[text()='Pages' or text()='Places']//following::button[@type='submit'][1]");
		String liked = likeButton.getAttribute("class");
		//If already liked, the class has two values		
		if(liked.contains("PageLikedButton") && liked.contains("PageLikeButton"))
		{
			System.out.println("Page Already Liked");
		}
		//if not, only one value
		else if (liked.contains("PageLikeButton"))
		{
			click(likeButton);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Like button clicked");
		}
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		click(searchResult);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (driver.getTitle().contains(searchItem))
		{
			System.out.println("Title "+driver.getTitle()+" matched "+searchItem);
		}
		else
		{
			System.out.println("Title "+driver.getTitle()+" did not match :"+searchItem);
		}
		WebElement noOfLikes = locateElement("xpath","(//span[text()='Community'])[2]//following::div[contains(text(),'people like this')]");
		String strNoOfLikes = noOfLikes.getText();
		strNoOfLikes = strNoOfLikes.substring(0, 5);
		System.out.println("No Of likes "+strNoOfLikes);
		closeBrowser();
	}
}
