package testcases;

import java.io.IOException;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static Object[][] read(String fileName) throws IOException {
		// TODO Auto-generated method stub
		XSSFWorkbook wbook = new XSSFWorkbook("./data/"+fileName+".xlsx");
		XSSFSheet sheet = wbook.getSheet("data");
		//getLastRowNum returns the number of rows excluding first row
		int rowCount = sheet.getLastRowNum();
		//System.out.println("number of rows "+rowCount);
		int colCount = sheet.getRow(0).getLastCellNum();
		//System.out.println("number of cols "+colCount);
		Object[][] data = new Object[rowCount][colCount];
		for (int i = 1; i <=rowCount; i++) {
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j <= colCount; j++) {
				XSSFCell cell = row.getCell(j);
					if(row.getCell(j)==null)
					{
						//row.getCell(j).setCellType(cell.CELL_TYPE_STRING);
						continue;
					}
					else
					{
						CellType cellTypeEnum;
						cellTypeEnum = cell.getCellTypeEnum();
						String value = "";
						if (cellTypeEnum==CellType.STRING)
						{
							//System.out.println(cell.getStringCellValue());
							value = cell.getStringCellValue();
						}
						else if (cellTypeEnum==CellType.NUMERIC)
						{
							//System.out.println(""+(long)cell.getNumericCellValue());
							value = ""+(long)cell.getNumericCellValue();
						}
						data[i-1][j] = value;
					}
				/*} catch (NullPointerException e) {
					// TODO Auto-generated catch block

					//cell.setCellValue("");
					System.err.println("Cell Value is Null");
					//e.printStackTrace();					
				}*/
					
			}
		}
		wbook.close();
		return data;
	}

}
