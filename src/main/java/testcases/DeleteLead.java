package testcases;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
//import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import testcases.ProjectMethod;

public class DeleteLead extends ProjectMethod{
	
	@BeforeClass(groups={"BeforeAndAfter"})
	public void assignTestVariables()
	{
		testCaseName = "Delete Lead";
		testDesc = "Delete an already created Lead in Leaftaps";
		author = "Stephy";
		category = "Sanity";
		fileName = "DeleteLead";
	}
	@Test(dataProvider="fetchData",groups= {"smoke"})
	public void deleteLeadMethod(String strFirstName)
	{
		WebElement eleLeads = locateElement("xpath", "//a[@href='/crmsfa/control/leadsMain']");
		click(eleLeads);
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		WebElement eleFindLeads = locateElement("xpath", "//a[text()='Find Leads']");
		click(eleFindLeads);
		WebElement eleFirstName = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(eleFirstName, strFirstName);
		WebElement eleButtonFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleButtonFindLeads);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//Identify the table and click on the first result
		WebElement table = locateElement("xpath","//table[@class='x-grid3-row-table']");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		//When used td.click, the link was not clicked. Only when traveled down to link it got clicked
		WebElement eleFirstResult = rows.get(0).findElements(By.tagName("td")).get(0).findElements(By.tagName("a")).get(0);
		String leadId = eleFirstResult.getText();
		click(eleFirstResult);
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement title = locateElement("xpath", "//div[@class='frameSectionExtra']/preceding-sibling::div");
		String strTitle = getText(title);		
		String pageTitle = "View Lead";
		if (pageTitle.equals(strTitle))
		{
			logSteps("Pass","Page title appeared is: "+strTitle+" as expected: "+pageTitle);
		}
		else
		{
			logSteps("Fail","Page title is not correct "+strTitle);
		}
		WebElement eleDelButton = locateElement("linktext", "Delete");
		click(eleDelButton);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Verify whether the lead Id present
		WebElement eleFindLeads1 = locateElement("xpath", "//a[text()='Find Leads']");
		click(eleFindLeads1);
		WebElement leadID = locateElement("xpath", "(//div[@class='x-tab-panel-bwrap']//input)[1]");
		type(leadID,leadId);
		//Click on Find Leads button
		WebElement findLeadsButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadsButton);
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement noRec = locateElement("xpath", "//div[@class='x-paging-info']");
		String noRecMsg = getText(noRec);
		String expMsg = "No records to display";
		if (noRecMsg.equals(expMsg))
		{
			logSteps("Pass","Passed: Lead Id is deleted successfully");
		}
		else
		{
			logSteps("Fail","Failed: Lead not deleted");
		}

		
	}
	
	}
