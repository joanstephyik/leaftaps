package testcases;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testcases.ProjectMethod;



public class MergeLead extends ProjectMethod{
		
	@BeforeClass(groups={"BeforeAndAfter"})
	public void assignTestVariables()
	{
		testCaseName = "Merge Lead";
		testDesc = "Merge two Leads in Leaftaps";
		author = "Stephy";
		category = "Sanity";
		fileName = "MergeLead";
	}
	
	@Test(dataProvider="fetchData")
	public void mergeLeadMethod(String fromLeadId,String toLeadId) throws InterruptedException
	{
		WebElement eleLeads = locateElement("xpath", "//a[@href='/crmsfa/control/leadsMain']");
		click(eleLeads);
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		WebElement eleMergeLead = locateElement("linktext", "Merge Leads");
		click(eleMergeLead);
		//entering first lead id 
		WebElement fromImg = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[1]");
		click(fromImg);
		switchToWindow(1);
		WebElement leadIDInWindow = locateElement("xpath", "//label[text()='Lead ID:']//following::input[1]");
		type(leadIDInWindow,fromLeadId);
		WebElement findLeadButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadButton);
		Thread.sleep(5000);
		//implicit wait didnt work here
		WebElement table = locateElement("xpath","(//table[@class='x-grid3-row-table'])");
		List<WebElement> rowlist = table.findElements(By.tagName("tr"));
		List<WebElement> collist = rowlist.get(0).findElements(By.tagName("td"));
		if (collist.get(0).getText().equals(fromLeadId)) 
		{
			collist.get(0).findElement(By.tagName("a")).click();
		}
		else
		{
			logSteps("Fail","Lead id is not found");
		}

		switchToWindow(0);
		//entering second lead id 
		WebElement toImg = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[2]");
		click(toImg);
		switchToWindow(1);
		leadIDInWindow = locateElement("xpath", "//label[text()='Lead ID:']//following::input[1]");
		type(leadIDInWindow,toLeadId);
		findLeadButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadButton);
		Thread.sleep(5000);
		//implicit wait did not work here
		table = locateElement("xpath","(//table[@class='x-grid3-row-table'])");
		rowlist = table.findElements(By.tagName("tr"));
		collist = rowlist.get(0).findElements(By.tagName("td"));
		if (collist.get(0).getText().equals(toLeadId)) 
		{
			collist.get(0).findElement(By.tagName("a")).click();
		}
		else
		{
			logSteps("Fail","Lead id is not found");
		}
		switchToWindow(0);
		//click on merge button
		WebElement mergeButton = locateElement("class", "buttonDangerous");
		click(mergeButton);
		Thread.sleep(2000);
		acceptAlert();
		
		//xPath by text method for find leads link
		WebElement findLeadsLink = locateElement("xpath", "//a[text()='Find Leads']");
		click(findLeadsLink);
		Thread.sleep(1000);
		//Enter lead ID
		WebElement leadID = locateElement("xpath", "(//div[@class='x-tab-panel-bwrap']//input)[1]");
		type(leadID,fromLeadId);
		//Click on Find Leads button
		WebElement findLeadsButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadsButton);
		Thread.sleep(7000);
		WebElement noRec = locateElement("xpath", "//div[@class='x-paging-info']");
		String noRecMsg = getText(noRec);
		String expMsg = "No records to display";
		if (noRecMsg.equals(expMsg))
		{
			logSteps("Pass","Passed No records to display is shown");
		}
		else
		{
			logSteps("Fail","Failed: Some records are displayed");
		}

	}
	
	@DataProvider(name="mergeDataProvider", indices= {0})
	public Object[][] provideDataMerge()
	{
		Object[][] inputMerge = new Object[3][2];
		
		inputMerge[0][0] = "11170";
		inputMerge[0][1] = "11175";
				
		inputMerge[1][0] = "11324";
		inputMerge[1][1] = "11132";
				
		inputMerge[2][0] = "11132";
		inputMerge[2][1] = "11145";
						
		
		return inputMerge;
	}
	
}
