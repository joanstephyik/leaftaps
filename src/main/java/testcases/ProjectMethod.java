package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;


public class ProjectMethod extends SeMethod{
	//Declare the test case name i.e before class variables here
	public String testCaseName, testDesc, author, category,fileName;
	
	@Parameters({"url","username","password","browser"})
	@BeforeMethod(groups={"BeforeAndAfter"})
	public void loginMethod(String url, String username,String pword, String browser) {
		
		//call the assign test method here, but the assignment of these variables will happen in the particular test like create, edit
		assignTest(testCaseName, testDesc, author, category, fileName); 
		if (browser.equals("chrome"))
		{
		startApp("chrome", url);
		}
		else if (browser.equals("firefox"))
		{
		startApp("firefox", url);
		}
		
		WebElement eleuserName = locateElement("id", "username");
		type(eleuserName, username);
		WebElement elepassword = locateElement("id", "password");
		type(elepassword, pword);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement eleCrmLink = locateElement("linktext", "CRM/SFA");
		click(eleCrmLink);
	}
	
	@AfterMethod(groups={"BeforeAndAfter"})
	public void logoutMethod()
	{
		closeBrowser();
	}
	
	@BeforeSuite(groups={"BeforeAndAfter"})
	public void beforeSuite()
	{
		startResult();
	}
	
	@AfterSuite(groups={"BeforeAndAfter"})
	public void afterSuite()
	{
		endResult();
	}
	
	@DataProvider(name="fetchData")
	public Object[][] provideData() throws IOException
	{
		return ReadExcel.read(fileName);
	}

}



