package testcases;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchSessionException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.support.ui.Select;

public class SeMethod extends HtmlReport implements WdMethods{
	
	public RemoteWebDriver driver ;
	
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")){
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")){
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			logSteps("pass","The browser: "+browser+"  launched successfully");
		} catch (UnreachableBrowserException e) {			
			logSteps("Fail", "The browser: "+browser+" could not be launched");
		}catch (WebDriverException e) {			
			logSteps("Fail", "The browser: "+browser+" could not be launched");
		}
	}


	public WebElement locateElement(String locator, String locValue) {
		
		try {
		switch (locator) {		
		case "id"	 : return driver.findElementById(locValue);
		case "class" : return driver.findElementByClassName(locValue);
		case "name" : return driver.findElementByName(locValue);
		case "linktext" : return driver.findElementByLinkText(locValue);
		case "partialLink" : return driver.findElementByPartialLinkText(locValue);
		case "tagname" : return driver.findElementByTagName(locValue);
		case "xpath" : return driver.findElementByXPath(locValue);
		case "cssSelect" : return driver.findElementByCssSelector(locValue);
		}	
		}
		catch (NoSuchElementException e) {
			logSteps("Fail"," locateElement: Element not found");
		}
		catch (WebDriverException e)
		{
			logSteps("Fail"," locateElement: An Error Occurred");
		}
		return null;
	}


	@Override
	public WebElement locateElement(String locValue) {
		
		try {
			WebElement ele = driver.findElementById(locValue);
			logSteps("Pass", "locateElement: Element "+ele.getText()+" is located");
			return ele;
		} catch (NoSuchElementException e) {
			
			logSteps("Fail", "locateElement: Element not found");
		}
		catch (WebDriverException e)
		{
			logSteps("Fail"," locateElement: An Error Occurred");
		}
		return null;
	}

	public List<WebElement> locateElements(String locator, String locValue) {
		List<WebElement> eleList = null;
		try {
			switch (locator) {
			case "id": eleList = driver.findElementsById(locValue);
			case "class": eleList =  driver.findElementsByClassName(locValue);
			case "linktext": eleList =  driver.findElementsByLinkText(locValue);
			case "xpath": eleList =  driver.findElementsByXPath(locValue);
			}
		} 
		catch (NoSuchElementException e) {
			
			logSteps("Fail"," locateElements: Element not found");
		}
		catch (WebDriverException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," locateElements: An Error Occurred");
		}		
		logSteps("Pass", "locateElements: Elements are located successfully and added to the list");
		return eleList;
	}
	
	public void clickWithNoSnap(WebElement ele) {
		String text = "";
		try {	
			text = ele.getText();
			ele.click();			
			logSteps("pass", "The element :"+text+"  is clicked.");
		} catch (InvalidElementStateException e) {
			logSteps("Fail", "The element: "+text+" could not be clicked");
		} catch (WebDriverException e) {
			logSteps("Fail", "Unknown exception occured while clicking in the field");
		} 
	}


	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			logSteps("Pass", "the Data "+data+" Entered Successfully");
		} catch (NoSuchElementException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," type: No Such Element");
		}
		catch (WebDriverException e) {
			//e.printStackTrace();
			logSteps("Fail"," type: Error Occured");
		}
	}

	@Override
	public void click(WebElement ele) {
		String text="";
		try {
			text = ele.getText();
			ele.click();			
			logSteps("Pass"," the Element "+text+" is clicked Successfully");
		} catch (NoSuchElementException e) {
			
			logSteps("Fail"," click: No Such Element");
		}
		catch (InvalidElementStateException e) {
			logSteps("Fail", "The element: "+text+" could not be clicked");
		}
		catch (WebDriverException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," click: Error Occured");
		}
	}

	@Override
	public String getText(WebElement ele) {
		
		
		try {
			String str = ele.getText();
			logSteps("Pass"," the text "+ele.getText()+" is read Successfully");
			return str;
			} 
		catch (NoSuchElementException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," getText: No Such Element");
		}
		catch (WebDriverException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," getText: Error Occured");
		}
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String text) {
		
		
		Select sel = new Select(ele);
		try {
			sel.selectByVisibleText(text);
			logSteps("Pass"," selectDropDownUsingText: The value "+text+" is selected from drop down");
		} catch (NoSuchElementException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," selectDropDownUsingText: No Such Element");
		}
		catch (WebDriverException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," Error occurred in selectDropDownUsingText");
		}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		
		Select sel = new Select(ele);
		try {
			sel.selectByIndex(index);
			logSteps("Pass"," selectDropDownUsingIndex: The value with index "+index+" is selected from drop down");
		} catch (NoSuchElementException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," selectDropDownUsingIndex: No Such Element");
		}
		catch (WebDriverException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," Error occurred in selectDropDownUsingIndex");
		}
	}
	
	public void selectDropDownUsingValue(WebElement ele, String value) {
		
		Select sel = new Select(ele);
		try {
			sel.selectByValue(value);
			logSteps("Pass"," selectDropDownUsingValue: The value "+value+" is selected from drop down");
		} catch (NoSuchElementException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," selectDropDownUsingValue: No Such Element");
		}
		catch (WebDriverException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," Error occurred in selectDropDownUsingValue");
		}
	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		
		String actTitle = driver.getTitle();
		try {
			if (actTitle.equals(expectedTitle)) 
			{
				System.out.println();
				logSteps("Pass"," verifyTitle: Actual title "+actTitle+" is as expected"+expectedTitle);
				return true;
			}
			
		} 
		catch (NoSuchSessionException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," verifyTitle: No such session exception");
		}
		catch (WebDriverException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," verifyTitle: Error Occurred");
		}
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		
		try {
			if (expectedText.equals(ele.getText()))
			{
				logSteps("Pass"," Webelement text is as expected "+ele.getText());
			}
			else
			{
				logSteps("Pass"," Expected text" +expectedText+ "didnt match with the actual text" +ele.getText());
			}
		}  catch (NoSuchElementException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," verifyExactText: No Such Element");
		}
		catch (WebDriverException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," Error occurred in verifyExactText");
		}
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		
		try {
			if (ele.getText().contains(expectedText))
			{
				logSteps("Pass"," Webelement contains the text "+expectedText+" expected "+ele.getText());
			}
			else
			{
				logSteps("Pass"," Actual text" +ele.getText()+ "didnt contain the actual text" +expectedText);
			}
		} catch (NoSuchElementException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," verifyPartialText: No Such Element");
		}
		catch (WebDriverException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," Error occurred in verifyPartialText");
		}
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		
		try {
			if (ele.getAttribute(attribute).equals(value))
			{
				logSteps("Pass"," Attribute "+attribute+" matched with the value "+value);
			}
			else
			{
				logSteps("Pass"," Attribute "+attribute+" not matched with the value "+value);
			}
		} catch (NoSuchElementException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," verifyExactAttribute: No Such Element");
		}
		catch (WebDriverException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," Error occurred in verifyExactAttribute");
		}
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		
		try {
			if (ele.getAttribute(attribute).contains(value))
			{
				logSteps("Pass"," Attribute "+attribute+" has the value "+value);
			}
			else
			{
				logSteps("Pass"," Attribute "+attribute+" does not the value "+value);
			}
		} catch (NoSuchElementException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," verifyPartialAttribute: No Such Element");
		}
		catch (WebDriverException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," Error occurred in verifyPartialAttribute");
		}

	}

	@Override
	public void verifySelected(WebElement ele) {
		
		try {

			if (ele.isSelected())
			{
				logSteps("Pass",ele.getText()+" selected");
			}
			else
			{
				logSteps("Pass",ele.getText()+" Not selected");
			}
		}
		catch (NoSuchElementException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," verifySelected: No Such Element");
		}
		catch (WebDriverException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," Error occurred in verifySelected");
		}

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		
		try {
			if (ele.isDisplayed())
			{
				logSteps("Pass"," Element is displayed");
			}
			else
			{
				logSteps("Pass"," Element is Not displayed");
			}
		} 
		catch (NoSuchElementException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," verifyDisplayed: No Such Element");
		}
		catch (WebDriverException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," Error occurred in verifyDisplayed");
		}
	}

	@Override
	public void switchToWindow(int index) {
		
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> windlist = new ArrayList<>();
		try {
			windlist.addAll(windowHandles);
			driver.switchTo().window(windlist.get(index));
			logSteps("Pass"," switchToWindow: Switched to window successfully");
		} 
		catch (NoSuchWindowException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," switchToWindow: No such window to switch");
		}
		catch (WebDriverException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," switchToWindow: Error occurred while trying to switch to window ");
		}
	}

	@Override
	public void switchToFrame(WebElement ele) {
		
		try {
			driver.switchTo().frame(ele);
			logSteps("Pass"," switchToFrame: Switched to frame successfully");
		} catch (NoSuchFrameException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," switchToFrame: No frame was present");
		} catch (WebDriverException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," switchToFrame: Error occured while switching to frame");
		}
	}

	@Override
	public void acceptAlert() {
		
		try {
			Alert alert = driver.switchTo().alert();
			alert.accept();
			logSteps("Pass"," acceptAlert: Alert accepted successfully",false);
		}
		catch (NoAlertPresentException e) {
			//e.printStackTrace();
			logSteps("Fail"," acceptAlert: No Alert was present",false);
		}
		catch(WebDriverException e){
			logSteps("Fail"," acceptAlert: Error occured while reading text from alert",false);
		}
	}

	@Override
	public void dismissAlert() {
		
		try {
			Alert alert = driver.switchTo().alert();
			alert.dismiss();
			logSteps("Pass"," dismissAlert: Alert dismissed successfully",false);
		}
		catch (NoAlertPresentException e) {
			//e.printStackTrace();
			logSteps("Fail"," dismissAlert: No Alert was present",false);
		}
		catch(WebDriverException e){
			logSteps("Fail"," dismissAlert: Error occured while reading text from alert",false);
		}
	}

	@Override
	public String getAlertText() {
		
		String alertText=null;
		try {
			Alert alert = driver.switchTo().alert();
			alertText = alert.getText();
			logSteps("Pass"," getAlertText: Alert text "+alertText+" is read successfully",false);

		} catch (NoAlertPresentException e) {
			//e.printStackTrace();
			logSteps("Fail"," getAlertText: No Alert was present",false);
		}
		catch(WebDriverException e){
			logSteps("Fail"," getAlertText: Error occured while reading text from alert",false);
		}
		return alertText;
	}
	
	@Override
	public long takeSnap() {
		long snapNumber = (long) Math.floor(Math.random()*900000000L) + 100000000L;
		
		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File fs = new File("./reports/snaps/img"+snapNumber+".png");
			FileUtils.copyFile(src, fs);
			
		} catch (IOException e) {
			
			//e.printStackTrace();
			
		}
		return snapNumber;
			}

	@Override
	public void closeBrowser() {
		
		try {
			driver.close();
			logSteps("Pass"," closeBrowser: Browser closed successfully",false);
		} 
		catch (NoSuchSessionException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," closeBrowser: Browser was already closed",false);
		}
		catch (WebDriverException e) {
			
			//e.printStackTrace();
			logSteps("Fail"," closeBrowser: Error while closing the browser",false);
		}
	}

	@Override
	public void closeAllBrowsers() {
		
		try {
			driver.quit();
			logSteps("Pass"," closeAllBrowsers: All Browsers closed successfully",false);
		} catch (WebDriverException e) {
			//e.printStackTrace();
			logSteps("Fail"," closeAllBrowsers: Error while closing the browsers",false);
		}
	}

}
