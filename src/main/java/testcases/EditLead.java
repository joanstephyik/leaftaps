package testcases;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
//import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import testcases.ProjectMethod;



public class EditLead extends ProjectMethod{

	@BeforeClass(groups={"BeforeAndAfter"})
	public void assignTestVariables()
	{
		testCaseName = "Edit Lead";
		testDesc = "Edit an already created Lead in Leaftaps";
		author = "Stephy";
		category = "Sanity";
		fileName = "EditLead";
	}
	
	@Test(dataProvider="fetchData",groups= {"sanity"},dependsOnGroups= {"smoke"})
	public void editLeadMethod(String strFirstName,String strUpdateCompany) throws InterruptedException
	{
		WebElement eleLeads = locateElement("xpath", "//a[@href='/crmsfa/control/leadsMain']");
		click(eleLeads);
		WebElement eleFindLeads = locateElement("xpath", "//a[text()='Find Leads']");
		click(eleFindLeads);
		WebElement eleFirstName = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(eleFirstName, strFirstName);
		WebElement eleButtonFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleButtonFindLeads);		
		//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		Thread.sleep(4000);
		//Identify the table and click on the first result
		WebElement table = locateElement("xpath","//table[@class='x-grid3-row-table']");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		//When used td.click, the link was not clicked. Only when traveled down to link it got clicked
		WebElement eleFirstResult = rows.get(0).findElements(By.tagName("td")).get(0).findElements(By.tagName("a")).get(0);
		click(eleFirstResult);
		WebElement title = locateElement("xpath", "//div[@class='frameSectionExtra']/preceding-sibling::div");
		String strTitle = getText(title);		
		String pageTitle = "View Lead";
		if (pageTitle.equals(strTitle))
		{
			logSteps("Pass", "Page title appeared is: "+strTitle+" as expected: "+pageTitle);
		}
		else
		{
			logSteps("Fail","Page title is not correct "+strTitle);
		}	
		WebElement eleEditButton = locateElement("linktext", "Edit");
		click(eleEditButton);
		WebElement companyName = locateElement("xpath", "//span[@class='requiredField']/following::input[1]");
		companyName.clear();
		type(companyName, strUpdateCompany);
		WebElement updateButton = locateElement("xpath","//input[@value='Update']");
		click(updateButton);
		WebElement updatedComp = locateElement("id", "viewLead_companyName_sp");
		String companyActual = getText(updatedComp);
		if (companyActual.contains(strUpdateCompany))
		{
			logSteps("Pass","Company name is changed as: "+companyActual);
		}
		else
		{
			logSteps("Fail","Company name is not correct");
		}

	}

}
