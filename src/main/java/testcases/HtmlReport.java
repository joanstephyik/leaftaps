package testcases;

import java.io.IOException;

//import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
//import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public abstract class HtmlReport {
	//These variables are going to be created only once so give as static
	static ExtentHtmlReporter html;
	static ExtentReports extent;
	//Each test its going to be varied hence normal
	ExtentTest test;
	public abstract long takeSnap();
    //Before Suite to be given in the project method annotation
	public void startResult() {
		//creates an empty html file
		html = new ExtentHtmlReporter("./Reports/extentReport.html");
		html.setAppendExisting(true);
		//load xml config from google for extent reports
		//html.loadConfig("./Config.xml");
		//creates an empty report
		extent = new ExtentReports();
		//attach reports to file
		extent.attachReporter(html);
	}
	
	//Before method 
	public void assignTest(String testCaseName, String testDesc, String author, String category, String fileName) 
	{
	//creates a test
	test = extent.createTest(testCaseName, testDesc);
	//assign author
	test.assignAuthor(author);
	//assign category
	test.assignCategory(category);
	}
	
	public void logSteps(String status, String log) 
	{
		logSteps(status, log, true);
	}
	//in try catch block
	public void logSteps(String status, String log,boolean bSnap) 
	{
		MediaEntityModelProvider snapshot = null;
		if (bSnap == true) {
		long snapPath = 1000000L;
		snapPath= takeSnap();
		
		try {
			snapshot = MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img"+snapPath+".png").build();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	if (status.equalsIgnoreCase("pass"))
	{
	test.pass(log,snapshot);
	}
	else if (status.equalsIgnoreCase("fail"))
	{
	test.fail(log,snapshot);
	}
	else if (status.equalsIgnoreCase("warning"))
	{
	test.warning(log,snapshot);
	}
	}
	//After suite
	public void endResult()
	{
		//save file
		extent.flush();
	}
	
}
