package week1.Day3;

import org.openqa.selenium.chrome.ChromeDriver;

public class DuplicateLead {
	
public static void main(String[] args) throws InterruptedException {
		
		Thread.sleep(3000);
		findAndDuplicateLeads("hoax@gmail.com","Duplicate Lead");
		
	}
	//Perform search in find leads section
	public static void findAndDuplicateLeads(String emailAddress, String pageTitle) throws InterruptedException 
	{
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		//instantiate chrome object
		ChromeDriver driver = new ChromeDriver();
		//Launch URL
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		//Login Username and password
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		//click on login
		driver.findElementByClassName("decorativeSubmit").click();
		//click on link
		driver.findElementByLinkText("CRM/SFA").click();
		//click on leads link - attribute xpath
		driver.findElementByXPath("//a[@href='/crmsfa/control/leadsMain']").click();
		//click on find leads link
		//preceding
		driver.findElementByXPath("//a[@href='/crmsfa/control/mergeLeadsForm']/preceding::a[@href='/crmsfa/control/findLeads']").click();
		Thread.sleep(3000);
		//xPath by text method
		//driver.findElementByXPath("//a[text()='Find Leads']").click();
		//Click on Email Tab
		driver.findElementByLinkText("Email").click();
		//Enter Email Address
		driver.findElementByXPath("//div[@class='x-panel-body x-panel-body-noheader x-panel-body-noborder']/div/div/input[@name='emailAddress']").sendKeys(emailAddress);
		//Click on Find Leads button
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		//Capture the name of first lead before duplicate action
		String companyName = driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-companyName']/a[text()='Test Leaf']").getText();
		//Click on first match
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1])").click();
		Thread.sleep(2000);
		//Get the Company Name before duplicate
		//String companyName = driver.findElementById("viewLead_companyName_sp").getText();
		//Click on duplicate lead button
		driver.findElementByLinkText("Duplicate Lead").click();
		Thread.sleep(2000);
		//Get the page title
		String title = driver.findElementByXPath("//div[@class='frameSectionExtra']/preceding-sibling::div").getText();
		if (pageTitle.equals(title))
		{
			System.out.println("Page title appeared is: "+title+" as expected: "+pageTitle);
		}
		else
		{
			System.out.println("Page title is not correct "+title);
		}	
		Thread.sleep(2000);
		
		//CLick on create Lead button
		driver.findElementByXPath("//span[contains(text(),'Postal Code')]/following::input[@value='Create Lead']").click();
		
		Thread.sleep(2000);
		String companyActual= driver.findElementById("viewLead_companyName_sp").getText();

		if (companyActual.contains(companyName))
		{
			System.out.println("Company name is "+companyName+" same as previous value "+companyActual);
		}
		else
		{
			System.out.println("Company name is not correct");
		}
		driver.close();	
	}
	


}
