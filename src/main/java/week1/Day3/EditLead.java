package week1.Day3;

import org.openqa.selenium.chrome.ChromeDriver;


public class EditLead {
	
	public static void main(String[] args) throws InterruptedException {
		
		Thread.sleep(3000);
		findAndEditLeads("Smart","View Lead","Verizon");
		
	}
	//Perform search in find leads section
	public static void findAndEditLeads(String firstName, String pageTitle,String companyName) throws InterruptedException 
	{
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		//instantiate chrome object
		ChromeDriver driver = new ChromeDriver();
		//Launch URL
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		//Login Username and password
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		//click on login
		driver.findElementByClassName("decorativeSubmit").click();
		//click on link
		driver.findElementByLinkText("CRM/SFA").click();
		//click on leads link - attribute xpath
		driver.findElementByXPath("//a[@href='/crmsfa/control/leadsMain']").click();
		//click on find leads link
		//preceding
		driver.findElementByXPath("//a[@href='/crmsfa/control/mergeLeadsForm']/preceding::a[@href='/crmsfa/control/findLeads']").click();
		Thread.sleep(3000);
		//xPath by text method
		//driver.findElementByXPath("//a[text()='Find Leads']").click();
		//Enter value in first name field
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys(firstName);
		//Click on Find Leads button
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		//Click on first match
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1])").click();
		//Get the page title
		String title = driver.findElementByXPath("//div[@class='frameSectionExtra']/preceding-sibling::div").getText();
		if (pageTitle.equals(title))
		{
			System.out.println("Page title appeared is: "+title+" as expected: "+pageTitle);
		}
		else
		{
			System.out.println("Page title is not correct "+title);
		}	
		Thread.sleep(2000);
		
		//CLick on edit button
		driver.findElementByLinkText("Edit").click();
		
		//Clear the old company name
		driver.findElementByXPath("//span[@class='requiredField']/following::input[1]").clear();
		//Enter Company name
		driver.findElementByXPath("//span[@class='requiredField']/following::input[1]").sendKeys(companyName);
		//click on update button
		driver.findElementByXPath("//input[@value='Update']").click();
		Thread.sleep(2000);
		String companyActual= driver.findElementById("viewLead_companyName_sp").getText();

		if (companyActual.contains(companyName))
		{
			System.out.println("Company name is changed as: "+companyActual);
		}
		else
		{
			System.out.println("Company name is not correct");
		}
		driver.close();	
	}
	

}
