package week1.Day2;

//import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IrctcSignUp {
	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		//instantiate chrome object
		ChromeDriver driver = new ChromeDriver();
		//Launch URL
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		//Login Username and password
		driver.findElementById("userRegistrationForm:userName").sendKeys("Joan");
		//check availability
		//driver.findElementByLinkText("Check Availability").click();
		driver.findElementById("userRegistrationForm:password").sendKeys("pass123_p09");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("pass123_p09");
		WebElement securQn = driver.findElementById("userRegistrationForm:securityQ");
		Select selSecure = new Select(securQn);
		selSecure.selectByValue("2");
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Scooby");		

		WebElement language = driver.findElementById("userRegistrationForm:prelan");
		Select sellan = new Select(language);
		sellan.selectByValue("hi");

		//Personal Details
		driver.findElementById("userRegistrationForm:firstName").sendKeys("StevenJo");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("One");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Family");
		driver.findElementById("userRegistrationForm:gender:1").click();
		driver.findElementById("userRegistrationForm:maritalStatus:0").click();
		//Select date of birth
		WebElement day = driver.findElementById("userRegistrationForm:dobDay");
		Select selDay = new Select(day);
		selDay.selectByValue("09");
		//Select Month
		WebElement month = driver.findElementById("userRegistrationForm:dobMonth");
		Select selMon = new Select(month);
		selMon.selectByVisibleText("JUN");
		//Select Year
		WebElement year = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select selYear = new Select(year);
		selYear.selectByIndex(10);
		//Select Occupation 
		WebElement occup = driver.findElementById("userRegistrationForm:occupation");
		Select selOccup = new Select(occup);
		selOccup.selectByVisibleText("Private");
		driver.findElementById("userRegistrationForm:uidno").sendKeys("123456789017256");
		driver.findElementById("userRegistrationForm:idno").sendKeys("DKPPS7220J");
		//Select Country
		WebElement country = driver.findElementById("userRegistrationForm:countries");
		Select selCount = new Select(country);
		selCount.selectByValue("94");
		driver.findElementById("userRegistrationForm:email").sendKeys("useremail@gmail.com");
		//driver.findElementById("userRegistrationForm:isdCode").sendKeys("91");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9597394270");
		//Select Nationality
		WebElement nation = driver.findElementById("userRegistrationForm:nationalityId");
		Select selNat = new Select(nation);
		selNat.selectByValue("94");
		//Residential Address
		driver.findElementById("userRegistrationForm:address").sendKeys("Plot No 24,Sri Sathiya Sai Nagar");
		driver.findElementById("userRegistrationForm:street").sendKeys("4th street");
		driver.findElementById("userRegistrationForm:area").sendKeys("Urapakkam");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("603210", Keys.TAB);
		Thread.sleep(3000);
		/*String stateValue = driver.findElementById("userRegistrationForm:statesName").getAttribute("value");
		if (stateValue.equalsIgnoreCase("Tamil Nadu"))
		{
			System.out.println("State populated properly");
		} 
		else 
		{
			System.out.println("Not populated correctly");
		}
		System.out.println("This is the state value"+stateValue);*/
		//Select City
		WebElement city = driver.findElementById("userRegistrationForm:cityName");
		Select selCity = new Select(city);
		selCity.selectByIndex(1);
		Thread.sleep(3000);
		//Select PostOffice
		WebElement post = driver.findElementById("userRegistrationForm:postofficeName");
		
		Select selPost = new Select(post);
		/*List<WebElement> optionsAvailable = selPost.getOptions();
		WebElement list1 = optionsAvailable.get(1);	
		System.out.println("All Options"+list1.getAttribute("name"));*/
		selPost.selectByIndex(1);
		//selPost.selectByVisibleText("Urapakkam S.O");
		
		driver.findElementById("userRegistrationForm:landline").sendKeys("0442487956");
		driver.findElementById("userRegistrationForm:resAndOff:0").click();
		//Thread.sleep(10000);
		//enter captcha manually
		driver.findElementByLinkText("Submit Registration Form>>>").click();
		
		
	}
}
