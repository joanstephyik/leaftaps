package week1.Day2;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
       //To invoke chrome broswer, set the value for driver from the project path
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		//instantiate chrome object
		ChromeDriver driver = new ChromeDriver();
		//Launch URL
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		//Login Username and password
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		//click on login
		driver.findElementByClassName("decorativeSubmit").click();
		//click on link
		driver.findElementByLinkText("CRM/SFA").click();
		//Click on Create Lead and enter mandatory fields
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("Test Leaf");
		driver.findElementById("createLeadForm_firstName").sendKeys("Smart");
		driver.findElementById("createLeadForm_lastName").sendKeys("UserOne");
		
		//Select from dropdown source
		WebElement source = driver.findElementById("createLeadForm_dataSourceId");
		Select selSource = new Select(source);
		selSource.selectByIndex(2);
		//Select from dropdown Marketing
		WebElement market = driver.findElementById("createLeadForm_marketingCampaignId");
		Select selMarket = new Select(market);
		selMarket.selectByValue("CATRQ_AUTOMOBILE");
		//Select from dropdown Preferred Currency
		WebElement curren = driver.findElementById("createLeadForm_currencyUomId");
		Select selcurren = new Select(curren);
		selcurren.selectByVisibleText("AOK - Angolan Kwanza");
		
		//Click on Create Lead button
		driver.findElementByClassName("smallSubmit").click();
		//close the browser
		driver.close();
		
	}

}
