package week1.Day2;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLeadHomeWork {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
	       //To invoke chrome broswer, set the value for driver from the project path
			System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
			//instantiate chrome object
			ChromeDriver driver = new ChromeDriver();
			//Launch URL
			driver.get("http://leaftaps.com/opentaps/");
			driver.manage().window().maximize();
			//Login Username and password
			driver.findElementById("username").sendKeys("DemoSalesManager");
			driver.findElementById("password").sendKeys("crmsfa");
			//click on login
			driver.findElementByClassName("decorativeSubmit").click();
			//click on link
			driver.findElementByLinkText("CRM/SFA").click();
			//Click on Create Lead and enter all fields
			driver.findElementByLinkText("Create Lead").click();
			driver.findElementById("createLeadForm_companyName").sendKeys("Test Leaf");
			driver.findElementById("createLeadForm_firstName").sendKeys("Smart");
			driver.findElementById("createLeadForm_lastName").sendKeys("UserOne");
			//Select from dropdown source
			WebElement source = driver.findElementById("createLeadForm_dataSourceId");
			Select selSource = new Select(source);
			selSource.selectByIndex(2);
			//Select from dropdown Marketing
			WebElement market = driver.findElementById("createLeadForm_marketingCampaignId");
			Select selMarket = new Select(market);
			selMarket.selectByValue("CATRQ_AUTOMOBILE");
			driver.findElementById("createLeadForm_firstNameLocal").sendKeys("LocalOne");
			driver.findElementById("createLeadForm_lastNameLocal").sendKeys("LocalLast");
			driver.findElementById("createLeadForm_personalTitle").sendKeys("Salutation");
			driver.findElementById("createLeadForm_generalProfTitle").sendKeys("ProfileTitle");
			driver.findElementById("createLeadForm_annualRevenue").sendKeys("50000");
			driver.findElementById("createLeadForm_departmentName").sendKeys("Instrumentation");
			//Select from drop down Preferred Currency
			WebElement curren = driver.findElementById("createLeadForm_currencyUomId");
			Select selcurren = new Select(curren);
			selcurren.selectByVisibleText("AOK - Angolan Kwanza");
			//Select from Industry Drop down
			WebElement Ind = driver.findElementById("createLeadForm_industryEnumId");
			Select IndSel = new Select(Ind);
			IndSel.selectByValue("IND_DISTRIBUTION");
			//Select from Ownership drop down
			WebElement owner = driver.findElementById("createLeadForm_ownershipEnumId");
			Select ownsel = new Select(owner);
			ownsel.selectByIndex(5);			
			driver.findElementById("createLeadForm_numberEmployees").sendKeys("500");
			driver.findElementById("createLeadForm_sicCode").sendKeys("60053");
			driver.findElementById("createLeadForm_tickerSymbol").sendKeys("####");
			driver.findElementById("createLeadForm_description").sendKeys("I dont want to enter any description");
			driver.findElementById("createLeadForm_importantNote").sendKeys("No Important notes");
			//Contact Info Section
			driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("506");
			driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9998856235");
			driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("6023510");
			driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("John Dewey");
			driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("+91");
			driver.findElementById("createLeadForm_primaryEmail").sendKeys("hoax@gmail.com");
			driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("http://leaftaps.com/opentaps/");
			//Primary Address section
			driver.findElementById("createLeadForm_generalToName").sendKeys("Joan");
			driver.findElementById("createLeadForm_generalAttnName").sendKeys("Venba");
			driver.findElementById("createLeadForm_generalAddress1").sendKeys("4/420 Plot no 24 Sathya Nagar, Ponmalai");
			driver.findElementById("createLeadForm_generalAddress2").sendKeys("7th Cross Jeevan Nagar");
			driver.findElementById("createLeadForm_generalCity").sendKeys("Trichy");
			//Select from state drop down
			WebElement selState = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
			Select sel = new Select(selState);
			sel.selectByVisibleText("Delaware");
			//Select from Country
			WebElement selCont = driver.findElementById("createLeadForm_generalCountryGeoId");
			Select selcon = new Select(selCont);
			selcon.selectByVisibleText("United States");
			
			driver.findElementById("createLeadForm_generalPostalCode").sendKeys("603210");
			driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("210");
			
			//Click on Create Lead button
			driver.findElementByClassName("smallSubmit").click();
			//close the browser
			driver.close();
	}

}
