package week1.Day5;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ListClassWork {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        List<String> ls = new ArrayList<>();
        ls.add("Stephy");
        ls.add("Pansey");
        ls.add("Stephen");
        ls.add("Venba");
        ls.add("George");
        ls.add("Jessica");
        ls.add("Flora");
        ls.add("Kingsly");
        ls.add("Stephy");
        ls.add("Stephy");
        ls.add("Stephy");
        
        for (String string : ls) {
			System.out.println(string);
		}
        Set<String> set = new HashSet<String>();
        //add all the values from list to set
        //set.addAll(ls);
        //add values one by one
        for (String str1 : ls) {
        	set.add(str1);
		}
        System.out.println(set);   
        
	}

}
