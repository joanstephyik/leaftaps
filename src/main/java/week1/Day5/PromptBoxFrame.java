package week1.Day5;

import org.openqa.selenium.Alert;
//import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class PromptBoxFrame {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		//instantiate chrome object
		ChromeDriver driver = new ChromeDriver();
		//Launch URL
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window().maximize();
		Thread.sleep(2000);
		//pass id as argument for frame method
		//driver.switchTo().frame("iframeResult");
		//pass as id for frame
		driver.switchTo().frame(0);
		//pass as web element
		//WebElement frame = driver.findElementById("iframeResult");
		//driver.switchTo().frame(frame);
		driver.findElementByXPath("//button[text()='Try it']").click();
		Alert alert = driver.switchTo().alert();
		alert.sendKeys("Stephy");
		alert.accept();
		String expText = driver.findElementById("demo").getText();
		if (expText.contains(("Stephy")))
		{
			System.out.println("Success");
		}
		else
		{
			System.out.println("Failed");
		}
		driver.switchTo().parentFrame();

	}

}
