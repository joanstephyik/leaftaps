package week1.Day5;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLeadWindowHandle {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		//instantiate chrome object
		ChromeDriver driver = new ChromeDriver();
		//Launch URL
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		//Login Username and password
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		//click on login
		driver.findElementByClassName("decorativeSubmit").click();
		//click on link
		driver.findElementByLinkText("CRM/SFA").click();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		//Thread.sleep(3000);
		//click on leads link - attribute xpath
		driver.findElementByXPath("//a[@href='/crmsfa/control/leadsMain']").click();
		//click on Merge Leads link
		driver.findElementByLinkText("Merge Leads").click();
		//Thread.sleep(2000);
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		//click on from lead look up
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[1]").click();
		Thread.sleep(2000);
		//create set for window handle
		Set<String> setH = driver.getWindowHandles();
		List<String> listH = new ArrayList<String>();
		//convert it into a list
		listH.addAll(setH);
		//switch to first tab
		driver.switchTo().window(listH.get(1));
		//enter first name and search
		driver.findElementByXPath("(//label[text()='First name:']/following::input)[1]").sendKeys("Selvam");
		driver.findElementByXPath("//button[text()='Find Leads']").click();	
		Thread.sleep(2000);
		//click on first result
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]").click();
		//switch to parent tab
		driver.switchTo().window(listH.get(0));
		//do the same in second look up
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[2]").click();
		Thread.sleep(2000);
		Set<String> setH1 = driver.getWindowHandles();
		List<String> listH2 = new ArrayList<String>();
		listH2.addAll(setH1);
		driver.switchTo().window(listH2.get(1));
		driver.findElementByXPath("(//label[text()='First name:']/following::input)[1]").sendKeys("testa");
		driver.findElementByXPath("//button[text()='Find Leads']").click();	
		Thread.sleep(2000);
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]").click();
		Thread.sleep(1000);	
		driver.switchTo().window(listH.get(0));
		//click on merge button
		driver.findElementByClassName("buttonDangerous").click();
		Alert alert = driver.switchTo().alert();
		alert.accept();
		//driver.switchTo().window(listH.get(0));
		//xPath by text method for find leads link
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		Thread.sleep(1000);
		//Enter lead ID
		driver.findElementByXPath("(//div[@class='x-tab-panel-bwrap']//input)[1]").sendKeys("10023");
		//Click on Find Leads button
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(7000);
		String noRecMsg = driver.findElementByXPath("//div[@class='x-paging-info']").getText();
		String expMsg = "No records to display";
		if (noRecMsg.equals(expMsg))
		{
			System.out.println("Passed No records to display is shown");
		}
		else
		{
			System.out.println("Failed: Some records are displayed");
		}
	}

}
