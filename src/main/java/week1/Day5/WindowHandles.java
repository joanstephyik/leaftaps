package week1.Day5;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandles {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		//instantiate chrome object
		ChromeDriver driver = new ChromeDriver();
		//Launch URL
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElementByLinkText("Contact Us").click();
		//window handles returned in a set - linkedhashset
		Set<String> handles = driver.getWindowHandles();
		//we want to traverse through it so convert it to list
		List<String> ls = new ArrayList<String>();
		//Add all the values to list
		ls.addAll(handles);	
		//driver focus will be still on parent tab so it closes parent tab
		driver.close();
		//manually switch focus to child tab
		driver.switchTo().window(ls.get(1));
		//get the title
		System.out.println(driver.getTitle());
				
	}

}
