package week1.Day4;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertClassRoom {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String nameEntered = "Joan";
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		//instantiate chrome object
		ChromeDriver driver = new ChromeDriver();
		//Launch URL
		driver.get("http://leafground.com/pages/Alert.html");
		driver.manage().window().maximize();
		driver.findElementByXPath("//button[text()='Prompt Box']").click();
		Alert alertObj = driver.switchTo().alert();
		alertObj.sendKeys(nameEntered);
		alertObj.accept();
		String nameDisplayed = driver.findElementById("result1").getText();
		if (nameDisplayed.contains(nameEntered))
		{
			System.out.println("Name entered "+nameEntered+" is same as name displayed: " +nameDisplayed);
		}
		else
		{
			System.out.println("Name entered "+nameEntered+" is NOT same as name displayed: " +nameDisplayed);
		}
		
	}

}
