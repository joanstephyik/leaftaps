package week1.Day4;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertMergeLeads {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		Thread.sleep(100);
		alertMergeLeads("11634","11631", "No records to display");
	}
	public static void alertMergeLeads(String fromLead,String toLead,String expMsg) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		//instantiate chrome object
		ChromeDriver driver = new ChromeDriver();
		//Launch URL
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		//Login Username and password
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		//click on login
		driver.findElementByClassName("decorativeSubmit").click();
		//click on link
		driver.findElementByLinkText("CRM/SFA").click();
		Thread.sleep(3000);
		//click on leads link - attribute xpath
		driver.findElementByXPath("//a[@href='/crmsfa/control/leadsMain']").click();
		//click on Merge Leads button
		driver.findElementByLinkText("Merge Leads").click();
		Thread.sleep(2000);
		//Enter value in from and To leads
		driver.findElementById("ComboBox_partyIdFrom").sendKeys(fromLead);
		driver.findElementById("ComboBox_partyIdTo").sendKeys(toLead);
		driver.findElementByClassName("buttonDangerous").click();
		Thread.sleep(3000);
		//Working with alert
		Alert alertObj = driver.switchTo().alert();
		String alertText = alertObj.getText();
		System.out.println("Alert Text displayed is: "+alertText);
		Thread.sleep(2000);
		alertObj.accept();
		//xPath by text method for find leads link
		driver.findElementByXPath("//a[text()='Find Leads']").click();
		Thread.sleep(1000);
		//Enter lead ID
		driver.findElementByXPath("(//div[@class='x-tab-panel-bwrap']//input)[1]").sendKeys(fromLead);
		//Click on Find Leads button
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(9000);
		String noRecMsg = driver.findElementByXPath("//div[@class='x-paging-info']").getText();
		if (noRecMsg.equals(expMsg))
		{
			System.out.println("Passed No records to display is shown");
		}
		else
		{
			System.out.println("Failed: Some records are displayed");
		}
	}
}
