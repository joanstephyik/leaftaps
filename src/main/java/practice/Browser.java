package practice;

//import java.io.File;
//import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchWindowException;
//import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class Browser {
public static void main(String[] args) {
	System.setProperty("webdriver.gecko.driver","./Drivers/geckodriver.exe");
	FirefoxDriver driver = new FirefoxDriver();
	//driver.close();
	driver.getTitle();
	driver.get("https://www.google.co.in/");
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
	driver.findElementByXPath("//input[@type='text']").sendKeys("Stephy Joan",Keys.ENTER);
	List<WebElement> list = driver.findElementsByPartialLinkText("Stephy Joan");
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	Actions build = new Actions(driver);
	
	//build.sendKeys(list.get(1),Keys.CONTROL).click(list.get(1)).perform();
	build.keyDown(Keys.CONTROL).click(list.get(1)).keyUp(Keys.CONTROL).perform();
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	Set<String> windowHandles = driver.getWindowHandles();
	List<String> windlist = new ArrayList<>();
	try {
		windlist.addAll(windowHandles);
		driver.switchTo().window(windlist.get(1));
		//driver.close();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//driver.switchTo().window(windlist.get(1));
		driver.quit();
		
	} 
	catch (NoSuchWindowException e) {
		// TODO Auto-generated catch block
		//e.printStackTrace();
		System.err.println("No such window to switch ");
	}
	catch (Exception e) {
		// TODO Auto-generated catch block
		//e.printStackTrace();
		System.err.println("Error occurred while trying to switch to window ");
	}
	finally
	{
		//File screenshotAs = driver.getScreenshotAs(OutputType.FILE);
		//File dsc = new File("./Snaps/img"+i+".png");
	}
}
public void parentMethod()
{
	ChildClass childObj = new ChildClass();
	childObj.methodName();
}
}
